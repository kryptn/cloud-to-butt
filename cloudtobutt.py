import time
import static
import praw


user_agent = "Cloud to Butt for comments by /u/kryptn v0.1"
user_name  = static.user_name
user_pword = static.user_pword

r = praw.Reddit(user_agent=user_agent)
r.login(user_name,user_pword)

def get_threads(limit=100):
    subs = r.get_subreddit('all').get_top(limit=limit)

    submissions = []
    # still need to invalidate visited threads
    for s in subs:
        if time.mktime(time.gmtime()) - s.created_utc > static.age_base:
            if s.num_comments > static.comment_base:
                submissions.append(s)

    return submissions

def parse_comments(submission):
#    submission.replace_more_comments(threshold=1)
    forest = submission.comments

    for c in forest[:-1]:
        if c.body.find(static.string_find) > 0:
            print c.body.replace(static.string_find,static.string_replace)

#    return forest


def test():
    threads = get_threads()
    f = parse_comments(threads[0])
    return f
