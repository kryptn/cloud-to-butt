#Cloud to Butt#

##Reddit implementation of cloud to butt##

### Uses: ###
* Replaces a string found in a comment with another string in a reply
* Annoy and amuse

### To Do: ###
* ~~Comment parsing~~
* ~~Figure out multiple subreddits~~
* Store comments made
* allow user-by-user ignore
* handle replies (Or don't?)
* handle mentions (Or also don't?)
* run unattended
* Figure out multiple base replacements (regex?)
* implement database because i'll need it anyway

### Ideas: ###
* run on a thread after aged enough or scored high enough
* only default subreddits or /r/all (can I even do all?)
* user-set replacements for x amount of time based on changetip?
* x comments per denomination of whatever, or time
* definiton of a mature thread? 1000+ comments? 12 hours? both?
